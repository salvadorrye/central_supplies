from django.db import models
from django.urls import reverse 
from geriatrics.models import Admission
from django.conf import settings

# Create your models here.
class Brand(models.Model):
    name = models.CharField(max_length=255, db_index=True)
    slug = models.SlugField(max_length=255, unique=True)

    class Meta:
        ordering = ('name',)
        verbose_name = 'brand'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('central_supplies:product-list-by-brand', args=[self.slug])
        
class Category(models.Model):
    name = models.CharField(max_length=200, db_index=True)
    slug = models.SlugField(max_length=200, unique=True)

    class Meta:
        ordering = ('name',)
        verbose_name = 'category'
        verbose_name_plural = 'categories'

    def __str__(self):
        return self.name

    def	get_absolute_url(self):
        return reverse('central_supplies:product-list-by-category',
                       args=[self.slug])

class Product(models.Model):
    category = models.ForeignKey(Category,
                                 related_name='products',
                                 on_delete=models.CASCADE)
    name = models.CharField(max_length=200, db_index=True)
    brand = models.ForeignKey(Brand, 
                              related_name='products',
                              on_delete=models.CASCADE,
                              null=True,
                              blank=True)
    slug = models.SlugField(max_length=200, db_index=True)
    image = models.ImageField(upload_to='products/%Y/%m/%d', blank=True)
    description = models.TextField(blank=True)
    standard = models.PositiveIntegerField(null=True)
    stock = models.PositiveIntegerField()
    #price = models.DecimalField(max_digits=10, decimal_places=2)
    available = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('name',)
        index_together = (('id', 'slug'),)

    def __str__(self):
        return self.name

    def	get_absolute_url(self):
        return reverse('central_supplies:product-detail',
                       args=[self.id, self.slug])

class SuppliedResident(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    resident = models.ForeignKey(Admission, on_delete=models.CASCADE, limit_choices_to={'discharged': False})
    standard = models.PositiveIntegerField()
    stock = models.PositiveIntegerField()
    created = models.DateTimeField(auto_now_add=True, null=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('product',)

    def __str__(self):
        return f'{self.resident} - {self.product}'

class SuppliedAccount(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    account = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    standard = models.PositiveIntegerField()
    stock = models.PositiveIntegerField()
    created = models.DateTimeField(auto_now_add=True, null=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('product',)

    def __str__(self):
        return f'{self.account} - {self.product}'

