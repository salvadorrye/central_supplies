import pytest
from mixer.backend.django import mixer
pytestmark = pytest.mark.django_db

class TestPost:
    def test_model(self):
        category = mixer.blend('central_supplies.Category')
        assert category.pk == 1, 'Should create a Cagegory instance'
        product = mixer.blend('central_supplies.Product', category=category)
        assert product.pk == 1, 'Should create a Product instance'

    def test_string_representation(self):
        category = mixer.blend('central_supplies.Category', name='medical supplies')
        assert str(category) == 'medical supplies', 'Should return a string representation'
        product = mixer.blend('central_supplies.Product', category=category, name='micropore')
        assert str(product) == 'micropore', 'Should return correct string representation'

    def test_get_absolute_url(self):
        category = mixer.blend('central_supplies.Category', slug='medical-supplies')
        assert category.get_absolute_url() == '/central-suppliesmedical-supplies/', 'Should return correct absolute url'
        product = mixer.blend('central_supplies.Product', category=category, slug='micropore')
        assert product.get_absolute_url() == f'/central-supplies{product.id}/micropore/', 'Should return correct absolute url'


