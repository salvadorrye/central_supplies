from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from django.test import LiveServerTestCase
import unittest

class NewVisitorTest(LiveServerTestCase):
    fixtures = ['central_supplies/user-data.json']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        #cls.selenium = webdriver.Chrome('/home/pi/chromedriver/chromedriver')
        cls.selenium = webdriver.Chrome()
        cls.selenium.implicitly_wait(10)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()
    
    def test_central_supplies_index_page(self):
        self.selenium.get(self.live_server_url + '/central-supplies')
        self.assertIn('Central Supplies', self.selenium.title)

    def test_admin_site(self):
        #User opens web browser, navigates to admin page
        self.selenium.get(self.live_server_url + '/admin/')
        body = self.selenium.find_element_by_tag_name('body').text
        self.assertIn('Django administration', body)
        #User types in username and password and presses enter
        username_field = self.selenium.find_element_by_name('username')
        username_field.send_keys('admin')
        password_field = self.selenium.find_element_by_name('password')
        password_field.send_keys('admin')
        password_field.send_keys(Keys.RETURN) 
        #Login credentials are correct, and the user is redirected to the main admin page
        body = self.selenium.find_element_by_tag_name('body').text
        self.assertIn('Site administration', body)
        self.assertIn('Categories', body)
        self.assertIn('Products', body)
        #User clicks on Categories link
        categories_link = self.selenium.find_element_by_link_text('Categories')
        categories_link.click()
        #User clicks on the Add categories link
        add_category_link = self.selenium.find_element_by_link_text('Add category')
        add_category_link.click()
        #User fills out the form
        self.selenium.find_element_by_name('name').send_keys('medical supply')
        self.selenium.find_element_by_name('slug').send_keys('medical-supply')
        #User clicks the save button
        self.selenium.find_element_by_css_selector("input[value='Save']").click()
        #The category has been added
        body = self.selenium.find_element_by_tag_name('body')
        self.assertIn('medical supply', body.text)
        #User clicks on Products link
        products_link = self.selenium.find_element_by_link_text('Products')
        products_link.click()
        #User clicks on the Add product
        add_product_link = self.selenium.find_element_by_partial_link_text('Add product')
        add_product_link.click()
        #User finds the category in the dropdown
        el = self.selenium.find_element_by_name('category')
        for option in el.find_elements_by_tag_name('option'):
            if option.text == 'medical supply':
                option.click()
        self.selenium.find_element_by_name('name').send_keys('micropore')
        self.selenium.find_element_by_name('slug').send_keys('micropore')
        self.selenium.find_element_by_name('stock').send_keys('1')
        self.selenium.find_element_by_name('price').send_keys('80')
        #User clicks the save button
        self.selenium.find_element_by_css_selector("input[value='Save']").click()
        #The product has been added
        body = self.selenium.find_element_by_tag_name('body')
        self.assertIn('micropore', body.text)
        #User logs out
        self.selenium.find_element_by_link_text('Log out').click()
        body = self.selenium.find_element_by_tag_name('body')
        self.assertIn('Thanks for spending some quality time with the Web site today.', body.text)

        #User can create a new item
        #User can click on any of the items and see the details
        #User can click one of the categories in the sidebar

