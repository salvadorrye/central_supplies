from django.apps import AppConfig


class CentralSuppliesConfig(AppConfig):
    name = 'central_supplies'

    def ready(self):
        import central_supplies.signals

