
# Central Supplies Web App
## A Django web app to keep track of our company's central supplies inventory

### Installation
1. Clone from Gitlab, install requirements and incorporate web app into project:
~~~~
mkdir mysite
cd mysite
virtualenv .cs -p /usr/bin/python3
source .cs/bin/activate
git clone https://gitlab.com/salvadorrye/central_supplies
pip install -r central_supplies/requirements.txt
django-admin startproject mysite .
~~~~

2. Edit `mysite/settings.py` and add `'central_supplies.apps.CentralSuppliesConfig'` to `INSTALLED_APPS`:
~~~~
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'central_supplies.apps.CentralSuppliesConfig',
]
~~~~

3. Include `'central_supplies.urls'` in your project's URLs. Append the following lines in your `mysite/urls.py`:
~~~~
from django.urls import include
urlpatterns += [
    path('central-supplies', include('central_supplies.urls')),
]
~~~~

