from django.shortcuts import render

# Create your views here.
from django.shortcuts import get_object_or_404
from .models import Category, Product
from cart.forms import CartAddProductForm
from cart.cart import Cart

def product_list(request, category_slug=None):
    category = None
    categories = Category.objects.all()
    products = Product.objects.filter(available=True)
    cart_product_form = CartAddProductForm()
    cart = Cart(request)
    if category_slug:
        category = get_object_or_404(Category, slug=category_slug)
        products = products.filter(category=category)
    return render(request, 'central_supplies/product/list.html',
            {'title': 'Products',
             'category': category,
             'categories': categories,
             'products': products,
             'cart_product_form': cart_product_form,
             'cart': cart
             })

def product_detail(request, id, slug):
    product = get_object_or_404(Product, id=id, slug=slug, available=True)
    cart_product_form = CartAddProductForm()
    cart = Cart(request)
    return render(request, 'central_supplies/product/detail.html',
            {'title': 'Product Detail',
             'product': product,
             'cart_product_form': cart_product_form,
             'cart': cart
             })

