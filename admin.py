from django.contrib import admin
from django.contrib.admin import AdminSite
from .models import Category, Product, Brand, SuppliedResident, SuppliedAccount
from geriatrics.models import Resident
from orders.models import Order, OrderItem
from core.admin import ExportCsvMixin
from cart.cart import Cart
from django.contrib import messages
from django.shortcuts import redirect

class CentralSuppliesAdminSite(AdminSite):
    site_header = 'St. Camillus MedHaven Central Supplies Admin'
    site_title = 'St. Camillus MedHaven Central Supplies Admin Portal'
    index_title = 'Welcome to St. Camillus MedHaven Central Supplies Portal'

central_supplies_admin_site = CentralSuppliesAdminSite(name='central_supplies_admin')

# Register your models here.
@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin, ExportCsvMixin):
    search_fields = ['name']
    list_display = ['name', 'slug']
    prepopulated_fields = {'slug': ('name',)}
    actions = ["export_as_csv"]

@admin.register(SuppliedAccount)
class SuppliedAccountAdmin(admin.ModelAdmin):
    list_display = ['account', 'product', 'standard', 'stock', 'request', 'created', 'updated']
    list_filter = ['account', 'product']
    list_editable = ['stock']

    def request(self, obj):
        return obj.standard - obj.stock

class SuppliedAccountInline(admin.TabularInline):
    model = SuppliedAccount

@admin.register(SuppliedResident)
class SuppliedResidentAdmin(admin.ModelAdmin):
    list_display = ['resident', 'product', 'standard', 'stock', 'request', 'created', 'updated']
    list_filter = ['resident', 'product']
    list_editable = ['stock']
    actions = ['add_requests_to_order']

    def request(self, obj):
        return obj.standard - obj.stock

    def add_requests_to_order(self, request, queryset):
        if 'resident__id__exact' in request.GET:
            resident_id = request.GET['resident__id__exact'].strip()
            resident = Resident.objects.get(id=resident_id).admission
            order = Order(resident=resident)
            total_orders = 0
            for query in queryset:
                product = query.product
                quantity = query.standard - query.stock
                if product.stock >= quantity and quantity > 0:
                    total_orders += 1
                elif quantity < 1:
                    messages.warning(request, f'{product} was not added to order! Requested quantity was [0] or less.')
                else:
                    messages.warning(request, f'{product} was not added to order! \
                            Quantity requested [{quantity}] is more than the stocks available \
                            [{product.stock}]. Please contact supplier.')
                print(total_orders)
            if total_orders > 0:
                order.save()
                for query in queryset:
                    product = query.product
                    quantity = query.standard - query.stock
                    if product.stock >= quantity and quantity > 0:
                        order_item = OrderItem(order=order, product=product, quantity=quantity)
                        order_item.save()
                messages.success(request, f'{order} CREATED SUCCESSFULLY!')
            else:
                messages.error(request, f'NO ORDER CREATED!')
        else:
            messages.error(request, f'Please filter by resident first before adding selected requests to order')

class SuppliedResidentInline(admin.TabularInline):
    model = SuppliedResident

@admin.register(Product)
class ProductAdmin(admin.ModelAdmin, ExportCsvMixin):
    search_fields = ['name', 'brand__name']
    list_display = ['name', 'slug', 'category', 'standard', 'stock', 'available', 'consumption', 'created', 'updated']
    list_filter = ['available', 'created', 'updated', 'category']
    list_editable = ['stock']
    prepopulated_fields = {'slug': ('name',)}
    actions = ["export_as_csv"]
    inlines = [SuppliedResidentInline, SuppliedAccountInline]

    def consumption(self, obj):
        total = 0
        for order in obj.order_items.filter(order__forwarded=False):
            total += order.quantity
        return total

@admin.register(Brand)
class BrandAdmin(admin.ModelAdmin, ExportCsvMixin):
    search_fields = ['name',]
    list_display = ['name', 'slug']
    prepopulated_fields = {'slug': ('name',)}
    actions = ["export_as_csv"]

central_supplies_admin_site.register(Category, admin_class=CategoryAdmin)
central_supplies_admin_site.register(Product, admin_class=ProductAdmin)
central_supplies_admin_site.register(Brand, admin_class=BrandAdmin)
central_supplies_admin_site.register(SuppliedResident, admin_class=SuppliedResidentAdmin)
central_supplies_admin_site.register(SuppliedAccount, admin_class=SuppliedAccountAdmin)

