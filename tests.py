from django.test import TestCase, Client
from django.urls import resolve
from central_supplies.views import product_list

# Create your tests here.
class IndexPageTest(TestCase):
    def setUp(self):
        self.client_stub = Client()

    def test_csu_url_resolves_to_product_list_page_view(self):
        response = self.client_stub.get('/central-supplies')
        self.assertEquals(response.status_code, 200)
        found = resolve('/central-supplies')
        self.assertEqual(found.func, product_list)

